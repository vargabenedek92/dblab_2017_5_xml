<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:functx="http://www.functx.com"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        version="2.0">
  <xsl:output method="xml" indent="yes" />
  <xsl:template match="/video">
    <xsl:copy>
      <!-- see xsl:attribute to copy attributes of the element -->
      <xsl:attribute name="element-type" >
        <xsl:value-of select="@element-type" />
      </xsl:attribute>
      <xsl:apply-templates select="videos" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="videos">
    <xsl:copy>
      <!-- deep-copy all the records that are of type BluRay -->
      <xsl:attribute name="element-type" >
        <xsl:value-of select="@element-type" />
      </xsl:attribute>
      <xsl:copy-of select="record[type='B']" />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>