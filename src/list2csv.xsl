<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:functx="http://www.functx.com"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  version="2.0">
  <xsl:output method="text" />
  <xsl:template match="/">
    <xsl:apply-templates select="video/videos" />
  </xsl:template>
  <xsl:template match="videos[@element-type='recordset']"
>"Cim";"Rendezo";"Megjelenes datuma"
<xsl:apply-templates select="*"
/></xsl:template>
  <xsl:template match="videos[@element-type='recordset']/record">
    <xsl:text>&quot;</xsl:text>
    <xsl:apply-templates select="title" /><xsl:text>&quot;;&quot;</xsl:text>
    <xsl:apply-templates select="appearance/@date" /><xsl:text>&quot;;&quot;</xsl:text>
    <xsl:apply-templates select="director" /><xsl:text></xsl:text>
    <xsl:text>&quot;&#10;</xsl:text>
  </xsl:template>
  <xsl:template match="record/*" priority="0.4"
><xsl:value-of select="."
/></xsl:template>
</xsl:stylesheet>